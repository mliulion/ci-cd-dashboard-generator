# CI-CD Dashboard Generator

CI-CD Dashboard Generator is a Golang executable for generating a table which shows the default Gitlab badges

## Installation

Requirements:

* Docker
* Git
* Makefile

## Usage

```sh
# default input (services.yaml)
make run

# custom input
FILE='_AN_AWESOME_FILENAME_' make run
```

The output file is `out.md`


### Example
#### Output

```md
| `Repo                             ` | CI<br>CD | Last commit<br>DEVELOP | Last commit<br>TEST | Last commit<br>MAIN |
| :---------------------------------- | :------: | :--------------------: | :-----------------: | :-------------------: |
| [ci-cd-dashboard-generator][ci-cd-dashboard-generator-📂📂📂] | [🚀][ci-cd-dashboard-generator-🚀🚀🚀] | [![pipeline status][ci-cd-dashboard-generator-⛏️🚀🏷️] ![coverage report][ci-cd-dashboard-generator-⛏️✅🏷️]][ci-cd-dashboard-generator-⛏️⛏️⛏️] | [![pipeline status][ci-cd-dashboard-generator-🧨🚀🏷️] ![coverage report][ci-cd-dashboard-generator-🧨✅🏷️]][ci-cd-dashboard-generator-🧨🧨🧨] | [![pipeline status][ci-cd-dashboard-generator-🛡️🚀🏷️] ![coverage report][ci-cd-dashboard-generator-🛡️✅🏷️]][ci-cd-dashboard-generator-🛡️🛡️🛡️] |
| [ci-template-file][ci-template-file-📂📂📂] | [🚀][ci-template-file-🚀🚀🚀] | [![pipeline status][ci-template-file-⛏️🚀🏷️] ![coverage report][ci-template-file-⛏️✅🏷️]][ci-template-file-⛏️⛏️⛏️] | [![pipeline status][ci-template-file-🧨🚀🏷️] ![coverage report][ci-template-file-🧨✅🏷️]][ci-template-file-🧨🧨🧨] | [![pipeline status][ci-template-file-🛡️🚀🏷️] ![coverage report][ci-template-file-🛡️✅🏷️]][ci-template-file-🛡️🛡️🛡️] |


[ci-cd-dashboard-generator-📂📂📂]:https://gitlab.com/mliulion/ci-cd-dashboard-generator
[ci-cd-dashboard-generator-🚀🚀🚀]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/-/pipelines
[ci-cd-dashboard-generator-⛏️⛏️⛏️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/-/commits/develop
[ci-cd-dashboard-generator-⛏️🚀🏷️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/badges/develop/pipeline.svg
[ci-cd-dashboard-generator-⛏️✅🏷️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/badges/develop/coverage.svg
[ci-cd-dashboard-generator-🧨🧨🧨]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/-/commits/test
[ci-cd-dashboard-generator-🧨🚀🏷️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/badges/test/pipeline.svg
[ci-cd-dashboard-generator-🧨✅🏷️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/badges/test/coverage.svg
[ci-cd-dashboard-generator-🛡️🛡️🛡️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/-/commits/main
[ci-cd-dashboard-generator-🛡️🚀🏷️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/badges/main/pipeline.svg
[ci-cd-dashboard-generator-🛡️✅🏷️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/badges/main/coverage.svg


[ci-template-file-📂📂📂]:https://gitlab.com/sethgitlab/ci-template-file
[ci-template-file-🚀🚀🚀]:https://gitlab.com/sethgitlab/ci-template-file/-/pipelines
[ci-template-file-⛏️⛏️⛏️]:https://gitlab.com/sethgitlab/ci-template-file/-/commits/develop
[ci-template-file-⛏️🚀🏷️]:https://gitlab.com/sethgitlab/ci-template-file/badges/develop/pipeline.svg
[ci-template-file-⛏️✅🏷️]:https://gitlab.com/sethgitlab/ci-template-file/badges/develop/coverage.svg
[ci-template-file-🧨🧨🧨]:https://gitlab.com/sethgitlab/ci-template-file/-/commits/test
[ci-template-file-🧨🚀🏷️]:https://gitlab.com/sethgitlab/ci-template-file/badges/test/pipeline.svg
[ci-template-file-🧨✅🏷️]:https://gitlab.com/sethgitlab/ci-template-file/badges/test/coverage.svg
[ci-template-file-🛡️🛡️🛡️]:https://gitlab.com/sethgitlab/ci-template-file/-/commits/main
[ci-template-file-🛡️🚀🏷️]:https://gitlab.com/sethgitlab/ci-template-file/badges/main/pipeline.svg
[ci-template-file-🛡️✅🏷️]:https://gitlab.com/sethgitlab/ci-template-file/badges/main/coverage.svg


```

#### Visualization

| `Repo                             ` | CI<br>CD | Last commit<br>DEVELOP | Last commit<br>TEST | Last commit<br>MAIN |
| :---------------------------------- | :------: | :--------------------: | :-----------------: | :-------------------: |
| [ci-cd-dashboard-generator][ci-cd-dashboard-generator-📂📂📂] | [🚀][ci-cd-dashboard-generator-🚀🚀🚀] | [![pipeline status][ci-cd-dashboard-generator-⛏️🚀🏷️] ![coverage report][ci-cd-dashboard-generator-⛏️✅🏷️]][ci-cd-dashboard-generator-⛏️⛏️⛏️] | [![pipeline status][ci-cd-dashboard-generator-🧨🚀🏷️] ![coverage report][ci-cd-dashboard-generator-🧨✅🏷️]][ci-cd-dashboard-generator-🧨🧨🧨] | [![pipeline status][ci-cd-dashboard-generator-🛡️🚀🏷️] ![coverage report][ci-cd-dashboard-generator-🛡️✅🏷️]][ci-cd-dashboard-generator-🛡️🛡️🛡️] |
| [ci-template-file][ci-template-file-📂📂📂] | [🚀][ci-template-file-🚀🚀🚀] | [![pipeline status][ci-template-file-⛏️🚀🏷️] ![coverage report][ci-template-file-⛏️✅🏷️]][ci-template-file-⛏️⛏️⛏️] | [![pipeline status][ci-template-file-🧨🚀🏷️] ![coverage report][ci-template-file-🧨✅🏷️]][ci-template-file-🧨🧨🧨] | [![pipeline status][ci-template-file-🛡️🚀🏷️] ![coverage report][ci-template-file-🛡️✅🏷️]][ci-template-file-🛡️🛡️🛡️] |


[ci-cd-dashboard-generator-📂📂📂]:https://gitlab.com/mliulion/ci-cd-dashboard-generator
[ci-cd-dashboard-generator-🚀🚀🚀]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/-/pipelines
[ci-cd-dashboard-generator-⛏️⛏️⛏️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/-/commits/develop
[ci-cd-dashboard-generator-⛏️🚀🏷️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/badges/develop/pipeline.svg
[ci-cd-dashboard-generator-⛏️✅🏷️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/badges/develop/coverage.svg
[ci-cd-dashboard-generator-🧨🧨🧨]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/-/commits/test
[ci-cd-dashboard-generator-🧨🚀🏷️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/badges/test/pipeline.svg
[ci-cd-dashboard-generator-🧨✅🏷️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/badges/test/coverage.svg
[ci-cd-dashboard-generator-🛡️🛡️🛡️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/-/commits/main
[ci-cd-dashboard-generator-🛡️🚀🏷️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/badges/main/pipeline.svg
[ci-cd-dashboard-generator-🛡️✅🏷️]:https://gitlab.com/mliulion/ci-cd-dashboard-generator/badges/main/coverage.svg


[ci-template-file-📂📂📂]:https://gitlab.com/sethgitlab/ci-template-file
[ci-template-file-🚀🚀🚀]:https://gitlab.com/sethgitlab/ci-template-file/-/pipelines
[ci-template-file-⛏️⛏️⛏️]:https://gitlab.com/sethgitlab/ci-template-file/-/commits/develop
[ci-template-file-⛏️🚀🏷️]:https://gitlab.com/sethgitlab/ci-template-file/badges/develop/pipeline.svg
[ci-template-file-⛏️✅🏷️]:https://gitlab.com/sethgitlab/ci-template-file/badges/develop/coverage.svg
[ci-template-file-🧨🧨🧨]:https://gitlab.com/sethgitlab/ci-template-file/-/commits/test
[ci-template-file-🧨🚀🏷️]:https://gitlab.com/sethgitlab/ci-template-file/badges/test/pipeline.svg
[ci-template-file-🧨✅🏷️]:https://gitlab.com/sethgitlab/ci-template-file/badges/test/coverage.svg
[ci-template-file-🛡️🛡️🛡️]:https://gitlab.com/sethgitlab/ci-template-file/-/commits/main
[ci-template-file-🛡️🚀🏷️]:https://gitlab.com/sethgitlab/ci-template-file/badges/main/pipeline.svg
[ci-template-file-🛡️✅🏷️]:https://gitlab.com/sethgitlab/ci-template-file/badges/main/coverage.svg


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU GPLv2](https://choosealicense.com/licenses/gpl-2.0/)
