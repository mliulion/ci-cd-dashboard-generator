package app

import (
	"fmt"
)

type Service struct {
	Name     string `yaml:"name"`
	URL      string `yaml:"url"`
	Branches struct {
		Develop string `yaml:"develop"`
		Test    string `yaml:"test"`
		Main    string `yaml:"main"`
	} `yaml:"branches"`
}

type ServicesYaml struct {
	Services []Service `yaml:"services"`
}

type Ref struct {
	Key   string
	URL   string
	Label string
}

type Row struct {
	Repo    string
	CiCd    string
	Develop string
	Test    string
	Main    string
}

type Table struct {
	Rows string
	Refs string
}

type RefType int

const (
	REF_REPO RefType = iota
	REF_CI_CD
	REF_DEVELOP
	REF_DEVELOP_PIPELINE
	REF_DEVELOP_COVERAGE
	REF_TEST
	REF_TEST_PIPELINE
	REF_TEST_COVERAGE
	REF_MAIN
	REF_MAIN_PIPELINE
	REF_MAIN_COVERAGE
)

var REFS = []RefType{
	REF_REPO,
	REF_CI_CD,
	REF_DEVELOP,
	REF_DEVELOP_PIPELINE,
	REF_DEVELOP_COVERAGE,
	REF_TEST,
	REF_TEST_PIPELINE,
	REF_TEST_COVERAGE,
	REF_MAIN,
	REF_MAIN_PIPELINE,
	REF_MAIN_COVERAGE,
}

func (serv *Service) BranchName(branch Branch) string {
	switch branch {
	case BRANCH_DEVELOP:
		return serv.Branches.Develop
	case BRANCH_TEST:
		return serv.Branches.Test
	case BRANCH_MAIN:
		return serv.Branches.Main
	}
	return ""
}

func (ref *Ref) String() string {
	return fmt.Sprintf("[%s]:%s", ref.Key, ref.URL)
}

func (serv *Service) Key(icoA, icoB, icoC Ico) string {
	return fmt.Sprintf("%s-%s%s%s", serv.Name, icoA, icoB, icoC)
}

func (serv *Service) Ref(refType RefType) Ref {
	ret := Ref{}

	switch refType {

	case REF_REPO:
		ret.Key = serv.Key(ICO_REPO, ICO_REPO, ICO_REPO)
		ret.URL = serv.URL
		ret.Label = serv.Name

	case REF_CI_CD:
		ret.Key = serv.Key(ICO_PIPELINE, ICO_PIPELINE, ICO_PIPELINE)
		ret.URL = fmt.Sprintf("%s/-/pipelines", serv.URL)
		ret.Label = ICO_PIPELINE

	case REF_DEVELOP:
		ret = serv.RefCommits(BRANCH_DEVELOP)
	case REF_DEVELOP_PIPELINE:
		ret = serv.RefPipeline(BRANCH_DEVELOP)
	case REF_DEVELOP_COVERAGE:
		ret = serv.RefCoverage(BRANCH_DEVELOP)

	case REF_TEST:
		ret = serv.RefCommits(BRANCH_TEST)
	case REF_TEST_PIPELINE:
		ret = serv.RefPipeline(BRANCH_TEST)
	case REF_TEST_COVERAGE:
		ret = serv.RefCoverage(BRANCH_TEST)

	case REF_MAIN:
		ret = serv.RefCommits(BRANCH_MAIN)
	case REF_MAIN_PIPELINE:
		ret = serv.RefPipeline(BRANCH_MAIN)
	case REF_MAIN_COVERAGE:
		ret = serv.RefCoverage(BRANCH_MAIN)
	}

	return ret
}

func (serv *Service) RefPipeline(branch Branch) Ref {
	return Ref{
		Key:   serv.Key(branch.Ico(), ICO_PIPELINE, ICO_BADGE),
		URL:   fmt.Sprintf("%s/badges/%s/pipeline.svg", serv.URL, serv.BranchName(branch)),
		Label: "pipeline status",
	}
}

func (serv *Service) RefCoverage(branch Branch) Ref {
	return Ref{
		Key:   serv.Key(branch.Ico(), ICO_COVERAGE, ICO_BADGE),
		URL:   fmt.Sprintf("%s/badges/%s/coverage.svg", serv.URL, serv.BranchName(branch)),
		Label: "coverage report",
	}
}

func (serv *Service) RefCommits(branch Branch) Ref {
	return Ref{
		Key:   serv.Key(branch.Ico(), branch.Ico(), branch.Ico()),
		URL:   fmt.Sprintf("%s/-/commits/%s", serv.URL, serv.BranchName(branch)),
		Label: "commits",
	}
}

func (serv *Service) Repo() string {
	ref := serv.Ref(REF_REPO)
	return fmt.Sprintf("[%s][%s]", ref.Label, ref.Key)

}

func (serv *Service) CiCd() string {
	ref := serv.Ref(REF_CI_CD)
	return fmt.Sprintf("[%s][%s]", ref.Label, ref.Key)

}

func (serv *Service) BranchRow(branch Branch) string {

	var ref, refPipeline, refCoverage Ref

	switch branch {
	case BRANCH_DEVELOP:
		ref = serv.Ref(REF_DEVELOP)
		refPipeline = serv.Ref(REF_DEVELOP_PIPELINE)
		refCoverage = serv.Ref(REF_DEVELOP_COVERAGE)
	case BRANCH_TEST:
		ref = serv.Ref(REF_TEST)
		refPipeline = serv.Ref(REF_TEST_PIPELINE)
		refCoverage = serv.Ref(REF_TEST_COVERAGE)
	case BRANCH_MAIN:
		ref = serv.Ref(REF_MAIN)
		refPipeline = serv.Ref(REF_MAIN_PIPELINE)
		refCoverage = serv.Ref(REF_MAIN_COVERAGE)
	}

	return fmt.Sprintf("[![%s][%s] ![%s][%s]][%s]", refPipeline.Label, refPipeline.Key, refCoverage.Label, refCoverage.Key, ref.Key)

}

func (serv *Service) ToRow() Row {
	return Row{
		Repo:    serv.Repo(),
		CiCd:    serv.CiCd(),
		Develop: serv.BranchRow(BRANCH_DEVELOP),
		Test:    serv.BranchRow(BRANCH_TEST),
		Main:    serv.BranchRow(BRANCH_MAIN),
	}
}
