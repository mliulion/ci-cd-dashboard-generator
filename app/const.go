package app

type Branch int

const (
	BRANCH_DEVELOP Branch = iota
	BRANCH_TEST
	BRANCH_MAIN
)

type Ico string

const (
	ICO_REPO     = "📂"
	ICO_PIPELINE = "🚀"
	ICO_COVERAGE = "✅"
	ICO_RUN      = "▶️"
	ICO_BADGE    = "🏷️"

	ICO_DEVELOP = "⛏️"
	ICO_TEST    = "🧨"
	ICO_MAIN    = "🛡️"
)

func (branch Branch) Ico() Ico {
	switch branch {
	case BRANCH_DEVELOP:
		return ICO_DEVELOP
	case BRANCH_TEST:
		return ICO_TEST
	case BRANCH_MAIN:
		return ICO_MAIN
	}
	return ""
}
