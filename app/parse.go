package app

import (
	"fmt"
	"io/ioutil"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mliulion/ci-cd-dashboard-generator/lib/libFrame"
	"gopkg.in/yaml.v2"
)

func ParseServicesYaml(fileName string) ServicesYaml {

	prefix := fmt.Sprintf("[%s]", libFrame.GetFrame(1).Function)
	msj := ""

	msj = "starting..."
	log.Info(prefix + " " + msj)

	servYaml := ServicesYaml{}

	fileBytes, err := ioutil.ReadFile(fileName)

	if err != nil {
		msj := fmt.Sprintf("error: %v", err)
		log.Fatalf(prefix + " " + msj)

	}

	err = yaml.Unmarshal([]byte(fileBytes), &servYaml)
	if err != nil {
		msj := fmt.Sprintf("error: %v", err)
		log.Fatalf(prefix + " " + msj)

	}

	return servYaml
}
