MY_UID=$(shell id -u)
MY_GID=$(shell id -g)
MY_PWD=$(shell pwd)
MY_APP_NAME=$(shell basename ${MY_PWD} .git)


MY_ENV_FILE:=$(if $(FILE),--env FILE=${FILE},)


.PHONY: check
check:
	@echo '-------------------------------- make $@ --------------------------------'
	@echo MY_APP_NAME=${MY_APP_NAME}




.PHONY: docker-build
docker-build:
	@echo '-------------------------------- make $@ --------------------------------'
	docker build \
		--network host \
		--build-arg MY_UID=${MY_UID} \
		--build-arg MY_GID=${MY_GID} \
		--tag ${MY_APP_NAME} \
		--file Dockerfile \
		.







.PHONY: check-run
check-run: docker-build
	@echo '-------------------------------- make $@ --------------------------------'
	docker run \
		--rm \
		--name ${MY_APP_NAME} \
		--user="${MY_UID}:${MY_GID}" \
		--volume "${PWD}":/app \
		--volume "${PWD}/.cache":/.cache \
		--volume "${PWD}/.go":/go \
		--volume "${PWD}/.tmp":/tmp \
		--workdir /app \
		${MY_APP_NAME}  \
		ls -la /


.PHONY: run
run: docker-build
	@echo '-------------------------------- make $@ --------------------------------'
	docker run \
		--rm \
		--name ${MY_APP_NAME} \
		--user="${MY_UID}:${MY_GID}" \
		${MY_ENV_FILE}\
		--volume "${PWD}":/app \
		--volume "${PWD}/.cache":/.cache \
		--volume "${PWD}/.go":/go \
		--volume "${PWD}/.tmp":/tmp \
		--workdir /app \
		${MY_APP_NAME} \
		make go



.PHONY: go
go:
	@echo '-------------------------------- make $@ --------------------------------'
	go get ./...
	go run ./cmd/main.go


.PHONY: setup
setup: docker-build
	@echo '-------------------------------- make $@ --------------------------------'
	docker run \
		--rm \
		--name ${MY_APP_NAME} \
		--user="${MY_UID}:${MY_GID}" \
		--volume "${PWD}":/app \
		--volume "${PWD}/.cache":/.cache \
		--volume "${PWD}/.go":/go \
		--volume "${PWD}/.tmp":/tmp \
		--workdir /app \
		${MY_APP_NAME} \
		make init




.PHONY: init
init:
	@echo '-------------------------------- make $@ --------------------------------'
	git remote -v | \
	head -n1 | \
	sed -e 's~^origin\s\+~~g' -e 's~git@~~g' -e 's~[^;]*://~~g' -e 's~:~/~g' -e 's~\(.git\)\?\s(fetch)~~g' | \
	xargs -I {} go mod init {};
	mkdir -p ./cmd; (ls ./cmd/main.go  > /dev/null 2>&1 && echo "./cmd/main.go" exists!!!) || echo -e "package main\n\nfunc init(){\n}\n\nfunc main(){\n}" > ./cmd/main.go
