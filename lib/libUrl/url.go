package libUrl

import (
	"strings"
)

func GetCleanUrl(input string) string {

	return strings.TrimSuffix(input, ".git")

}
