package libFrame

import "runtime"

func GetFrame(level int) runtime.Frame {
	pc := make([]uintptr, 15)
	n := runtime.Callers(level+1, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return frame
}
