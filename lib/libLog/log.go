package libLog

import (
	"fmt"
	"log"
	"os"
	"runtime"

	"github.com/sirupsen/logrus"
	"gitlab.com/mliulion/ci-cd-dashboard-generator/lib/libFrame"
)

const DebugLevel = logrus.DebugLevel

const LogLevelEnvVarName = "LOG_LEVEL"

func InitLog(logLvl *logrus.Level) {

	prefix := fmt.Sprintf("[%s]", libFrame.GetFrame(1).Function)
	msj := ""

	msj = "starting..."
	log.Printf(prefix + " " + msj)

	logrus.SetOutput(os.Stdout)

	logrusLevel := logrus.InfoLevel

	if logLvl != nil {
		logrusLevel = *logLvl
	} else {
		logLevel, logLevelIsSet := os.LookupEnv(LogLevelEnvVarName)
		if !logLevelIsSet {
			fmt.Println("[WARNING]", LogLevelEnvVarName, "missing")
		} else {
			lvl, err := logrus.ParseLevel(logLevel)
			if err != nil {
				fmt.Println("[WARNING]", err)
			} else {
				logrusLevel = lvl
			}
		}
	}
	logrus.SetLevel(logrusLevel)

	logrus.SetReportCaller(true)
	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors:     true,
		FullTimestamp:   true,
		TimestampFormat: "2006-01-02T15:04:05+03:00",
		CallerPrettyfier: func(f *runtime.Frame) (function string, file string) {
			// myFuncStr := fmt.Sprintf("%s:", path.Base(f.Function))
			myFuncStr := ""
			return myFuncStr, fmt.Sprintf("%s:%d", f.File, f.Line)
		},
	})

	msj = "init logrus done"
	log.Printf(prefix + " " + msj)
}
