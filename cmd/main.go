package main

import (
	"bytes"
	"fmt"
	"html/template"
	"io"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mliulion/ci-cd-dashboard-generator/app"
	"gitlab.com/mliulion/ci-cd-dashboard-generator/lib/libFrame"
	"gitlab.com/mliulion/ci-cd-dashboard-generator/lib/libLog"
	"gitlab.com/mliulion/ci-cd-dashboard-generator/lib/libUrl"
)

func init() {
	prefix := fmt.Sprintf("[%s]", libFrame.GetFrame(1).Function)
	msj := ""

	msj = "starting..."
	log.Info(prefix + " " + msj)

	lvl := libLog.DebugLevel
	libLog.InitLog(&lvl)
}

const ENV_VARNAME_FILE = "FILE"

func main() {

	prefix := fmt.Sprintf("[%s]", libFrame.GetFrame(1).Function)
	msj := ""

	msj = "starting..."
	log.Info(prefix + " " + msj)

	yamlFileName := "services.yaml"

	envYaml, envYamlSet := os.LookupEnv(ENV_VARNAME_FILE)
	if envYamlSet {
		yamlFileName = envYaml
	}

	services := app.ParseServicesYaml(yamlFileName)

	msj = fmt.Sprintf("services: %#v", services)
	log.Trace(prefix + " " + msj)

	fileOut, err := os.Create("out.md")
	if err != nil {
		msj := fmt.Sprintf("error: %v", err)
		log.Fatalf(prefix + " " + msj)

	}
	defer fileOut.Close()

	fileHeader, err := os.Open("templates/md/table_header.md")
	if err != nil {
		msj := fmt.Sprintf("error: %v", err)
		log.Fatalf(prefix + " " + msj)

	}
	defer fileHeader.Close()

	bytesCopied, err := io.Copy(fileOut, fileHeader)

	if err != nil {
		msj := fmt.Sprintf("error: %v", err)
		log.Fatalf(prefix + " " + msj)

	}
	msj = fmt.Sprintf("bytesCopied: %#v", bytesCopied)
	log.Debug(prefix + " " + msj)

	var refBuff bytes.Buffer

	tRow, err := template.ParseFiles("templates/md/row.md")

	if err != nil {
		msj := fmt.Sprintf("error: %v", err)
		log.Fatalf(prefix + " " + msj)

	}

	for i, serv := range services.Services {
		prefix := fmt.Sprintf("%s[i:%#v]", prefix, i)

		name := serv.Name
		prefix = fmt.Sprintf("%s[name:%#v]", prefix, name)

		msj = "starting..."
		log.Debug(prefix + " " + msj)

		serv.URL = libUrl.GetCleanUrl(serv.URL)

		row := serv.ToRow()
		err := tRow.Execute(fileOut, row)
		if err != nil {
			msj := fmt.Sprintf("error: %v", err)
			log.Fatalf(prefix + " " + msj)

		}

		_, err = refBuff.WriteString("\n\n")

		if err != nil {
			msj := fmt.Sprintf("error: %v", err)
			log.Fatalf(prefix + " " + msj)

		}

		for j, refId := range app.REFS {
			prefix := fmt.Sprintf("%s[j:%#v]", prefix, j)

			ref := serv.Ref(refId)

			bytesWritten, err := refBuff.WriteString(ref.String() + "\n")

			if err != nil {
				msj := fmt.Sprintf("error: %v", err)
				log.Fatalf(prefix + " " + msj)

			}

			msj = fmt.Sprintf("bytesWritten: %#v", bytesWritten)
			log.Debug(prefix + " " + msj)
		}

	}
	bytesWritten, err := refBuff.WriteTo(fileOut)
	if err != nil {
		msj := fmt.Sprintf("error: %v", err)
		log.Fatalf(prefix + " " + msj)

	}

	msj = fmt.Sprintf("bytesWritten: %#v", bytesWritten)
	log.Debug(prefix + " " + msj)

	msj = "finish"
	log.Info(prefix + " " + msj)
}
